var todos = [],
    count = 1;

module.exports = (function(app) {
  app.route('/todos')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .post(function(req, res) {
      var todo;

      todo = JSON.parse(req.body.model);
      todo.id = count++;
      todos.push(todo);

      res.json(todo);
    })
    .get(function(req, res) {
      res.json(todos);
    });


  app.route('/todos/:id')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .get(function(req, res) {
      var id,
	  i;

      id = +req.params.id;
      for (i = 0; i < todos.length; i++) {
	if (todos[i].id === id) {
	  break;
	}
      }

      res.json(todos[i]);
    })
    .put(function(req, res) {
      var todo,
	  id,
	  i;

      id = +req.params.id;
      todo = JSON.parse(req.body.model);
      for (i = 0; i < todos.length; i++) {
	if (todos[i].id === id) {
	  break;
	}
      }

      todos[i].title = todo.title;
      todos[i].done = todo.done;

      res.json(todos[i]);
    })
    .delete(function(req, res) {
      var todo,
	  id,
	  i;

      id = +req.params.id;
      for (i = 0; i < todos.length; i++) {
	if (todos[i].id === id) {
	  break;
	}
      }

      todo = todos[i];
      todos.splice(i, 1);
      res.json(todo);
    });  
});
